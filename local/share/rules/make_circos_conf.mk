# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

CIRCOS_BASEDIR = $(shell echo $$(dirname $$($(call load_modules); which circos))/../)


# BANDS
define BANDS_CONF
show_bands            = yes
fill_bands            = yes
band_stroke_thickness = 2
band_stroke_color     = white
band_transparency     = 0
endef
export BANDS_CONF
bands.conf:
	@echo "$$BANDS_CONF" >$@



# IDEOGRAM
define IDEOGRAM_CONF
<ideogram>

<spacing>
default = 0.01r
break   = 0.5r
</spacing>

<<include ideogram.position.conf>>
<<include ideogram.label.conf>>
<<include bands.conf>>

</ideogram>
endef
export IDEOGRAM_CONF
ideogram.conf:
	@echo "$$IDEOGRAM_CONF" >$@







# IDEOGRAM LABEL
# position and size of chr labels
define IDEOGRAM_LABEL_CONF
show_label       = yes
label_font       = bold
label_radius     = dims(ideogram,radius_inner) - 100p
label_with_tag   = yes
label_size       = 48
label_parallel   = yes
label_case       = lower
#label_format     = eval(sprintf("chr%s",var(label)))
endef
export IDEOGRAM_LABEL_CONF
ideogram.label.conf:
	@echo "$$IDEOGRAM_LABEL_CONF" >$@





# IDEOGRAM POSITION
define IDEOGRAM_POSITION_CONF
radius           = 0.90r
thickness        = 100p
fill             = yes
fill_color       = black
stroke_thickness = 2
stroke_color     = black
endef
export IDEOGRAM_POSITION_CONF
ideogram.position.conf:
	@echo "$$IDEOGRAM_POSITION_CONF" >$@






# TICKS
define TICKS_CONF
show_ticks          = yes
show_tick_labels    = yes

<ticks>

radius           = dims(ideogram,radius_outer)+120p
orientation      = out
label_multiplier = 1e-6
color            = black
size             = 20p
thickness        = 3p
label_offset     = 5p

<tick>
spacing        = 1u
show_label     = no
</tick>

<tick>
spacing        = 5u
show_label     = yes
label_size     = 20p
format         = %d
</tick>

<tick>
spacing        = 10u
show_label     = yes
label_size     = 24p
format         = %d
</tick>

</ticks>
endef
export TICKS_CONF
ticks.conf:
	@echo "$$TICKS_CONF" >$@












# MAIN CONF FILE
define CIRCOS_CONF
<<include colors_fonts_patterns.conf>>

<<include ideogram.conf>>
<<include ticks.conf>>

<image>
dir   = .
file  = circos.png
png   = yes
svg   = no
# radius of inscribed circle in image
radius = 2000p

# by default angle=0 is at 3 o'clock position
angle_offset = -90

#angle_orientation = counterclockwise
auto_alpha_colors = yes
auto_alpha_steps  = 5
<<include $(CIRCOS_BASEDIR)/etc/background.white.conf>>
</image>

#karyotype   = $(CIRCOS_BASEDIR)/data/karyotype/karyotype.human.txt
karyotype   = karyotype.txt

chromosomes_units = 1000000
# V0chr2_random;
chromosomes       =  Schr?NUM?;V0chr?NUM?;V0chrUn;V0chr?NUM?_random
chromosomes_display_default = no
chromosomes_reverse = Schr?NUM?



# LINKS
<links>
<link>
ribbon = yes
twist  = no

file          = link.txt

radius        = 0.93r
bezier_radius = 0r

color            = green
stroke_color     = black
stroke_thickness = 2

# Limit how many links to read from file and draw
record_limit  = 10000
</link>
</links>




# Thick marker of the scaffolds
<plots>
<plot>
type  = text
color = blue
file  = text.bands.txt

r0    = 1r
r1    = 1r+300p

label_size = 30
label_font = condensed

show_links     = yes
link_dims      = 0p,2p,6p,2p,5p
link_thickness = 2p
link_color     = black

label_snuggle        = yes
max_snuggle_distance = 1r
snuggle_tolerance    = 0.25r
snuggle_sampling     = 2
# !!! snuggle_refine = yes cause infinite loop of refininment algorithm
snuggle_refine       = no
</plot>
# Thick marker of the scaffolds finish here

# Print orientation
<plot>
type       = scatter
file       = strand.txt
# If you set r0=r1, then both forward and reverse glyphs will overlap.
# If you set r0=r1, then both forward and reverse glyphs will overlap.
r0         = 0.99r
r1         = 0.99r
glyph      = triangle
glyph_size = 50p

# Adjust the rotation and color of each glyph, based on its value.
# By default, the triangle glyph points outward. 
# -90 deg - points counter-clockwise
#  90 deg - points clockwise
<rules>
<rule>
condition      = var(value) == -1
glyph_rotation = -90
color          = red
</rule>
<rule>
condition      = var(value) == 1
glyph_rotation = 90
color          = blue
</rule>
</rules>

<axes>
<axis>
position  = -1,1
color     = grey
thickness = 1
</axis>
</axes>
</plot>
# Print orientation end here


</plots>


<<include $(CIRCOS_BASEDIR)/etc/housekeeping.conf>>
data_out_of_range* = trim
endef
export CIRCOS_CONF



# MAIN CONF
# the configuration file content is adapted to the chr number
circos.%.conf: ideogram.conf ideogram.label.conf ideogram.label.conf ideogram.position.conf ticks.conf bands.conf
	echo "$$CIRCOS_CONF" >$@; \
	if [[ ":2:6:8:14:15:19:Un:" =~ ":$*:" ]]; then \
		sed -i -s 's/V0chr?NUM?_random//g' $@; \   * since not all V0 chrs have a corresponding random chr, remove them *
	fi; \
	sed -i -s 's/?NUM?/$*/g' $@   * change chr number in the config file *
