# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

# identify illumina contigs that spans gaps between scaffolds
# to fine zone that were not cloned into bac

extern ../phase_2/golden5.agp as GOLDEN_AGP
extern ../phase_2/golden5.fasta.gz as GOLDEN_FASTA_GZ


log:
	mkdir -p $@

# preserve gap number
sample.gaps.bed:
	bawk '!/^[\#+,$$]/ { if ( $$5 == "U" ) { print $$1, $$2-1, $$3, "gap_"$$4, $$6; } }' $(GOLDEN_AGP) >$@

sample.component.bed:
	bawk '!/^[\#+,$$]/ { if ( $$5 == "W" ) { print $$1, $$2-1, $$3, $$6, $$8, $$9; } }' $(GOLDEN_AGP) >$@

# sample.golden.fasta:
	# zcat <$(GOLDEN_FASTA_GZ) >$@

sample.golden.fasta:
	zcat <../../../../pn_update/dataset/datasets/golden5.masquered.gz >$@

reference.2bit:
	$(call load_modules); \
	faToTwoBit <(zcat <$(PN_ILLUMINA_CONTIGS)) $@


EXTENSION := 500

# extract a sequence of length EXTENSION from each of the scaffolds
# scaffolds flanching a given gap
sample.flanking.component.bed: sample.gaps.bed sample.component.bed
	$(call load_modules); \
	bedtools closest -io -D a -b <(bsort -k1,1 -k2,2n <$<) -a <(bsort -k1,1 -k2,2n <$^2) \
	| bawk '!/^[$$,\#+]/ { \
		if ( $$12 == "-1" ) { \
			LEFT=$$2; \
			RIGHT=$$3; \
			if ( $$5 > $(EXTENSION) ) { LEFT=RIGHT-$(EXTENSION); } \   * if there is not enough sequence, take the sequence length that remains *
			print $$1, LEFT, RIGHT, $$10":"$$4":left"; \
		} \
		if ( $$12 == "1" ) { \
			LEFT=$$2; \
			RIGHT=$$3; \
			if ( $$5 > $(EXTENSION) ) { RIGHT=LEFT+$(EXTENSION); } \
			print $$1, LEFT, RIGHT, $$10":"$$4":rigth"; \
		} \
	}' >$@

# extract regions from fasta
sample.flanking.component.fasta: sample.golden.fasta sample.flanking.component.bed
	$(call load_modules); \
	bedtools getfasta -fi $< -bed $^2 -fo stdout -name >$@

.META: sample.match.psl
	1	matches	Number of bases that match that aren't repeats
	2	misMatches	Number of bases that don't match
	3	repMatches	Number of bases that match but are part of repeats
	4	nCount	Number of "N" bases
	5	qNumInsert	Number of inserts in query
	6	qBaseInsert	Number of bases inserted in query
	7	tNumInsert	Number of inserts in target
	8	tBaseInsert	Number of bases inserted in target
	9	strand	"+" or "-" for query strand For translated alignments, second "+"or "-" is for genomic strand
	10	qName	Query sequence name
	11	qSize	Query sequence size
	12	qStart	Alignment start position in query
	13	qEnd	Alignment end position in query
	14	tName	Target sequence name
	15	tSize	Target sequence size
	16	tStart	Alignment start position in target
	17	tEnd	Alignment end position in target
	18	blockCount	Number of blocks in the alignment (a block contains no gaps)
	19	blockSizes	Comma-separated list of sizes of each block
	20	qStarts	Comma-separated list of starting positions of each block in query
	21	tStarts	Comma-separated list of starting positions of each block in target


11.ooc: log reference.2bit sample.flanking.component.fasta
	$(call load_modules); \
	/usr/bin/time -v \
	blat \
	-tileSize=11 \
	-minIdentity=98 \
	-noHead \
	-makeOoc=11.ooc \   * make overused tile file *
	-repMatch=1024 \   *  sets the number of repetitions of a tile allowed before it is marked as overused *
	$^2 $^3 tmp \
	2>&1 \
	| tee $</$@.mm.blat

sample.match.psl: log 11.ooc reference.2bit sample.flanking.component.fasta
	$(call load_modules); \
	/usr/bin/time -v \
	blat \
	-ooc=$^2 \   * Use overused tile file N.ooc *
	-tileSize=11 \
	-minIdentity=98 \
	-noHead \
	-repMatch=1024 \
	$^3 $^4 $@ \
	2>&1 \
	| tee $</$@.mm.blat


# select contigs that align both a left side and a right side
# of a given gap
spanned.gap: sample.match.psl
	bawk 'function abs(x){return ((x < 0.0) ? -x : x)} \   * return absolute value *
	!/^[$$,\#+]/ \
	{ if ( abs( $$qEnd - $$qStart ) > ( $(EXTENSION) * 0.50 ) ) { print $$qName, $$tName; } }' $< \   * filter alignments for length *
	| tr ":" \\t \
	| select_columns 4 1 3 \
	| sed 's/\t/:/1' \   * crate key tName:gap *
	| bsort -k 1,1 -k 2,2 \
	| collapsesets -g $$'\t' 2 \   * collapse non redundant positions for the same tName:gap *
	| bawk '{ if ( $$3 != "" ) { split($$1,a,":"); print a[2], a[1], $$2, $$3; }}' >$@   * select items with two non-redundant positions *

.META: spanning.match.psl
	1	gap
	2	tName
	3	qStart1
	4	qEnd1
	5	tStart1
	6	tEnd1
	7	tSize1
	8	strand1
	9	qName1
	10	position1
	11	qStart2
	12	qEnd2
	13	tStart2
	14	tEnd2
	15	tSize2
	16	strand2
	17	qName2
	18	position2

# return best match for each of the spanned.gap
# the alignments must be consistent for strand
spanning.match.psl: spanned.gap sample.match.psl
	bawk 'function abs(x){return ((x < 0.0) ? -x : x)} \
	!/^[$$,\#+]/ { \
	if ( abs( $$qEnd - $$qStart ) > ( $(EXTENSION) * 0.50 ) ) { \
		split($$qName,a,":"); \
		print a[1], a[2], a[3], $$tName, $$qStart, $$qEnd, $$tStart, $$tEnd, $$tSize, $$strand; }}' $^2 \
	| filter_2col 1 4 <(cut -f 1,2 $<) \   * filter for gap and contig *
	| bsort -k 1,1V -k 9,9n -k 4,4 -k 3,3 \   * sort for gap, qaln_len, tName, position *
	| bawk '{ print $$1":"$$4, $$5" "$$6" "$$7" "$$8" "$$9" "$$10" "$$11" "$$2" "$$3; }' \   * crate key gap:tName *
	| set_collapse -g ":" -o 2 \   * collapse for previous key *
	| tr " :" \\t \
	| bawk '{ if ( $$8 == $$17 ) print $$0; }' \   * select alignments consistent for strand *
	| sed 's/\t\t/\t/g' \
	| bsort -k 1,1 >$@

.IGNORE: spanning.seq.fasta
spanning.seq.fasta: spanning.match.psl
	zcat <$(PN_ILLUMINA_CONTIGS) \
	| fasta_get -f <(bawk '{ print $$tName }' $< | bsort) >$@




ALL += spanning.match.psl \
	spanned.gap \
	sample.match.psl

INTERMEDIATE += 

CLEAN +=