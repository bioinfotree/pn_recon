# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

vitis12XV2.agp:
	wget -O $@ https://urgi.versailles.inra.fr/content/download/4702/36526/file/vitis12XV2_final.agp

# vitis12XV2.contigs.fasta.gz:
	# wget -O $@ https://urgi.versailles.inra.fr/download/vitis/VV_12X_embl_102_WGS_contigs.fsa.zip

vitis12XV2.scaffolds.fasta.gz:
	wget -O $@ https://urgi.versailles.inra.fr/download/vitis/VV_12X_embl_102_Scaffolds.fsa.zip

vitis12XV2.chrs.fasta.gz:
	wget -O $@ https://urgi.versailles.inra.fr/download/vitis/12Xv2_grapevine_genome_assembly.fa.gz

vitis12XV2.chrs.renamed.fasta.gz: vitis12XV2.chrs.fasta.gz
	zcat $< \
	| sed '/^$$/d' \
	| fasta2tab \
	| sed -e 's/chrUkn/chrUn/' \
	| awk 'BEGIN { OFS="\t"; i=0; } \
	!/^[\#+,$$]/ { \
	split($$1,a," "); \
	print a[1], toupper($$NF); \
	}' \
	| bsort \
	| tab2fasta 2 \
	| gzip -c9 >$@

vitis12XV2.scaff.renamed.fasta.gz: vitis12XV2.scaffolds.fasta.gz
	zcat $< \
	| sed '/^$$/d' \
	| fasta2tab \
	| awk 'BEGIN { OFS="\t"; i=0; } \
	!/^[\#+,$$]/ { \
	split($$6,a,"."); \
	print $$1, a[1] >"scaff.map"; \
	print a[1], toupper($$NF); \
	}' \
	| bsort \
	| tab2fasta 2 \
	| gzip -c9 >$@

scaff.map: vitis12XV2.scaff.rename.fasta.gz
	touch $@

vitis12XV2.renamed.agp: scaff.map vitis12XV2.agp
	bawk '!/^[\#+,$$]/ { print $$0; }' $^2 \
	| translate -i -e "500" -v $< 6 >$@

vitis12XV2.bed: vitis12XV2.renamed.agp
	$(call load_modules); \
	python -m jcvi.formats.agp bed --nogaps $< >$@

vitis12XV2.component.bed: vitis12XV2.renamed.agp vitis12XV2.bed
	$(call load_modules); \
	python -m jcvi.formats.agp bed --nogaps --component $< \
	| translate -a <(select_columns 4 1 <$^2) 1 \
	| select_columns 1 3 4 2 6 7 >$@


vitis12XV0.bed:
	$(call load_modules); \
	python -m jcvi.formats.agp bed --nogaps $(PN_V0_AGP) \
	| bsort -k 1,1 -k 2,2n >$@

vitis12XV0.component.bed: vitis12XV0.bed
	$(call load_modules); \
	python -m jcvi.formats.agp bed --nogaps --component $(PN_V0_AGP) \
	| translate -a <(select_columns 4 1 <$<) 1 \
	| select_columns 1 3 4 2 6 7 >$@



.PHONY: test
test:
	@echo 


ALL += 

INTERMEDIATE += 

CLEAN += 